# Banner Variant Generator

### Quick start 

_**Try it**_ on your local machine by following these _**5; easy steps**_:

### 1. Clone & Install
Clone the repository & install dependencies by _copy-pasting_ the following command into your terminal:

##### Windows:
```sh
git clone https://git.tusisobar.com/banners/banner-variant-generator/ && cd banner-variant-generator && rmdir /s /q .git && npm install
```

##### MacOS:
```sh
git clone https://git.tusisobar.com/banners/banner-variant-generator/ && cd banner-variant-generator && rm -f -r .git && npm install
```

### 2. Input banners 

Copy your files into `/sample` directory following the folder structure
```
/sample
|-- /dist
    `-- /banner1
        `-- banner1_<WIDTH>x<HEIGHT>.zip
|-- /src
`-- /assets
    `-- /banner1
        |-- /<WIDTH>x<HEIGHT>
        `-- /<WIDTH>x<HEIGHT>
```

### 3. Update `package.json`

- `deployment`: The path to your deployment server folder
- `email`: The email to receive updates

### 4. Set up Git Repo & Jenkins

Ensure that Jenkins is fetching Pipeline script from your git repository.

Set up git remote:

```sh
git init && git remote add origin <GIT REPOSITORY>
```

### 5. Generate Bannerss

Generate your banners by _copy-pasting_ the following command:

```sh
git add . && git commit -m "Initial Commit" && git push -u origin master
```



