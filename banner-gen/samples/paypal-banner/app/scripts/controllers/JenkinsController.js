var fs = require ("fs");

function JenkinsController () {
	this.packageJson;
}

JenkinsController.prototype.getPackageJson = function () {
	if (this.packageJson) 
		return this.packageJson;

	try {
		var packageJsonFd = fs.openSync ("package.json", 'r')
	} catch (err) {
		return console.log (err);
	}

	try {
		var packageJson = fs.readFileSync (packageJsonFd);
	} catch (err) {
		return console.log(err);
	}

	this.packageJson = JSON.parse(packageJson);
	return this.getPackageJson ();
}

JenkinsController.prototype.getDeploymentServer = function () {
	var packageJson = this.getPackageJson();
	return packageJson["deployment"];
}

JenkinsController.prototype.getEmail = function () {
	var packageJson = this.getPackageJson();
	return packageJson["email"];
}

module.exports = new JenkinsController ();