var fs = require ("fs");
var archiver = require('archiver');
var decompress = require ("decompress");
var path = require ("path");

function Zipper () {}

Zipper.prototype.unzipFilesToFolder = function (
	src, subfolders, 
	target, outputFolder, 
	filter) {

	filter = filter || function (x) { return false; };
	if (Array.isArray(subfolders)) {
		var tmp_files = {};
		for (var i = 0; i < subfolders.length; i++) {
			tmp_files[subfolders[i]] = 1;
		}
		subfolders = tmp_files;
	}

	var promiseQueue = [];
	for (var subfolder in subfolders) {
		console.log ("Unzip: " + subfolder);
		promiseQueue.push(this.unzipFileToFolder (
			src, subfolders, subfolder, 
			target, outputFolder, 
			filter));
	}

	return Promise.all(promiseQueue)
		.then (function () { return Promise.resolve (subfolders); });
}

Zipper.prototype.unzipFileToFolder = function (
	src, subfolders, subfolder,
	target, outputFolder, 
	filter) {

	var self = this;
	return this.getZipFilePath (src, subfolder, filter)
			.then (function (filepath) {
				return Zipper
					.unzip (filepath, path.join(target, subfolder, outputFolder))
					.catch(err => {
						console.log(err);
					});
			})
			.catch (function (err) {
				if (err) {
					delete subfolders[subfolder];
					console.log("SOMETHING WRONG WITH: " + subfolder);
				}
			})
}

Zipper.prototype.getZipFilePath = function (src, subfolder, filter) {
	var folderpath = path.join(src, subfolder);
	var self = this;
	return new Promise (function (resolve, reject) {
		fs.readdir (folderpath, function (err, files) {
			if (err || files.length == 0) {
				err = err || new Error ("No files in dist folder");
				return reject (err);
			}

			for (var i = 0; i < files.length; i++) {
				if (!filter (files[i])) {
					return resolve (path.join (folderpath, files[i]));
				}
			}

			return reject (new Error ("No files in dist folder"));
		})
	})
}


Zipper.getArchive = function (resolve, reject, target) {
	
	var output = fs.createWriteStream(target);
	var archive = archiver('zip', {
	    zlib: { level: 9 } // Sets the compression level. 
	});

	// listen for all archive data to be written 
	output.on('close', Zipper.outputOnClose(archive, resolve));
	 
	// good practice to catch warnings (ie stat failures and other non-blocking errors) 
	archive.on('warning', function(err) {
	  if (err.code === 'ENOENT') {
	      // log warning 
	  } else {
	      // throw error 
	      reject(err);
	  }
	});
	 
	// good practice to catch this error explicitly 
	archive.on('error', function(err) {
	  reject(err);
	});
	 
	// pipe archive data to the file 
	archive.pipe(output);

	return archive;
}

Zipper.outputOnClose = function (archive, callback) {
	return function() {
		console.log(archive.pointer() + ' total bytes');
		console.log('archiver has been finalized and the output file descriptor has closed.');
		callback ();
	}
}


Zipper.zip = function (src, target) {
	// create a file to stream archive data to. 
	
	return Zipper.zipArray([src], target);
}

Zipper.zipArray = function (srcs, target) {
	return new Promise (function (resolve, reject) {
		var archive = Zipper.getArchive(resolve, reject, target);

		srcs.forEach(function (src) {
			// append a file from string 	
			archive.directory(src, false);
		})
		
		// finalize the archive (ie we are done appending files but streams have to finish yet) 
		archive.finalize();
	})
}

Zipper.unzip = function (src, target) {
	return decompress(src, target);
}


module.exports = Zipper;