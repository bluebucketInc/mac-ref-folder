	var ExtendedFileSystem = require ("../classes/ExtendedFileSystem");
var ExtendedFile = require ("../classes/ExtendedFile");
var Counter = require ("../classes/Counter");
var Size = require ("../classes/Size");
var Zipper = require ("./Zipper");

var util = require ("util");
var fs = require ("fs");
var path = require ("path");

function BannerCloner (banner, assets, temp, dist, outputFile) {
	this.banner = banner || path.resolve (__dirname, '../../../banner');
	this.assets = path.join (this.banner, (assets || '/assets'));
	this.temp = path.join (this.banner, (temp || '/preview'));
	this.dist = path.join (this.banner, (dist || '/dist'));
	this.outputFile = outputFile || '/index.html';
	this.template = '/template';
	this.tree = {};
}

BannerCloner.prototype.start = function () {
	var self = this;

	//console.log ("========= CLONE ASSETS ==============");
	// this.cloneAssets()
	// 	.then(function () {
	// 		console.log ("========= BUILD FOLDER TREE ==============");
	// 		return self.buildFolderTree (this.assets);
	// 	})
		self.buildFolderTree (this.assets)
		.then(function (tree) {
			console.log ("========= UNZIP BANNER TEMPLATES ==============");
			return new Zipper().unzipFilesToFolder(
				self.dist, tree, 
				self.temp, self.template, 
				function (file) {
					return !Size.isValid (file);
				}
			);
			//return self.unzipBannerTemplates (tree);
		})
		.then(function (tree) {
			var promises = [];
			console.log ("========= CLONE ASSETS AND EDIT TEMPLATES ==============");
			promises.push(self.cloneAndEditBannerHTML (tree));
			promises.push(self.cloneAssets (self.assets, tree));
			return Promise.all(promises).then (function () { return Promise.resolve(tree); });
		})
		.then(function (tree) { 
			console.log ("========= ZIP CLONED ==============");
			return self.zipClonedTemplates(tree);
		})
		.then(function (targets) {
			return self.zipZippedClonedTempaltes(targets);
		})
		.then(function () {
			console.log ("========= DONE! =========");
			process.exit();
		})
		.catch (function (err) {
			console.log(err);
		})
		
}

//////////////////////////////////////////////////////////
///                   CLONE ASSETS                     ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.cloneAssets = function (src, tree) {
	if (!tree)
		return this.cloneAssetsByFolder (src);
	else if (typeof tree == "object")
		return this.cloneAssetsByTree (src, tree);
	else return Promise.reject (new Error ("Don't know what to clone!"));

}

BannerCloner.prototype.cloneAssetsByTree = function (src, tree) {
	var self = this;
	var promises = [];
	for (var project in tree) {
		for (var size in tree[project]) {
			var _src = path.join(src, project, tree[project][size]);
			var _target = self.getClonedFilePath (self.temp, project, tree[project][size], "");

			promises.push (ExtendedFileSystem.clone (_src, _target, function (a) { return a; }))
		}
	}

	return Promise.all(promises);
}

BannerCloner.prototype.cloneAssetsByFolder = function (src) {
	var target = this.temp;
	var self = this;

	return ExtendedFileSystem.clone(src, target, function (extendedFile) {
		var filename = extendedFile.filename;
		var foldername = extendedFile.foldername;
		
		if (!self.isDimension(filename))
			return extendedFile;

		extendedFile.setFilename (self.getClonedDirName (foldername, filename));
		return extendedFile;

	});
}

//////////////////////////////////////////////////////////
///                BUILD FOLDER TREE                   ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.buildFolderTree = function (src) {
	var tree = {};
	return this.transverseDir (src, tree)
		.then(function () {
			return Promise.resolve(tree); 
		})
		.catch (function (err) {
			console.log(err);
		});
}

BannerCloner.prototype.transverseDir = function (src, tree) {
	var self = this;
	
	var readdirPromise = new Promise (function (resolve, reject) {
		fs.readdir(src, function (err, files) {
			if (err) {
				console.log(err);
				return reject(err);
			}
			resolve(files);
		})
	});

	return readdirPromise.then (function (files) {
		var promises = [];
		files.forEach (function (file) {
			if (ExtendedFileSystem.shouldIgnore (file))
				return;
			promises.push(self.transverseFile (src, tree, file));
		})
		return Promise.all(promises);
	})
}

BannerCloner.prototype.transverseFile = function (src, tree, file) {
	var src_path = path.join(src, file);
	var self = this;

	var statPromise = new Promise (function (resolve, reject) {

		fs.stat (src, function (err, stats) {
			if (err) {
				return reject (err);
			}

			if (stats.isDirectory()) {
				if (self.isDimension(file)) {
					self.addDimensionToProject (tree, self.extractProjectName(src), file);
					resolve ();
				} else {
					resolve (function () { return self.transverseDir (src_path, tree) });
				}
			} else {
				reject ();
			}
		});
	})

	return statPromise.then (function (fn) { if (fn) return fn(); })
	
}


//////////////////////////////////////////////////////////
///              CLONE AND EDIT BANNER                 ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.cloneAndEditBannerHTML = function (tree) {
	var dist_folder = this.dist;
	var temp_folder = this.temp;
	var outputFile = this.outputFile;
	var self = this;

	var promises = [];
	for (var project in tree) {
		var banner = path.join(this.temp, project, this.template, outputFile);
		var targets = [];
		tree[project].forEach (function (size) {
			targets.push (self.getClonedFilePath (temp_folder, project, size, outputFile));
		})

		promises.push(ExtendedFileSystem.clone (banner, targets, function (extendedFile) {
			return self.editHTML(extendedFile);
		}));
	}

	return Promise.all(promises)
		.then (function () {
			return Promise.resolve(tree);
		})
		.catch (err => { console.log (err); });
}

//////////////////////////////////////////////////////////
///               ZIP CLONED BANNERS                   ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.zipClonedTemplates = function (tree) {
	var promises = [];
	var targets = [];
	for (var project in tree) {
		for (var size in tree[project]) {
			var clonedDirName = this.getClonedDirName(project, tree[project][size]);
			var target = path.join (this.dist, project, clonedDirName + ".zip");
			promises.push(Zipper.zip (
				path.join (this.temp, project, clonedDirName), 
				target
			))
			target = path.relative(path.resolve(__dirname, "../../../"), target);
			console.log(target);
			targets.push(target);
		}
	}

	return Promise.all(promises).then(() => { return Promise.resolve (targets); });
}

//////////////////////////////////////////////////////////
///               ZIP CLONED BANNERS                   ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.zipZippedClonedTempaltes = function (srcs) {
	console.log(srcs);
	return Zipper.zipArray (
		srcs,
		path.join(this.dist, "banners_dist.zip")
	)
}

//////////////////////////////////////////////////////////
///                        MISC                        ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.getClonedDirName = function (foldername, filename) {
	return this.extractProjectName(foldername) + "-" + filename
}
BannerCloner.prototype.getClonedFilePath = function (temp, project, size, outputFile) {
	return path.join(temp, project, this.getClonedDirName(project, size), outputFile);
}
BannerCloner.prototype.isDimension = function (value) {
	return Size.isValid(value);
}


//////////////////////////////////////////////////////////
///                   FILE EDITOR                      ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.editHTML = function (extendedFile) {

	data = extendedFile.data
	target = extendedFile.getFullPath();
	var size = Size.fromString(target);
	var old_size = this.extractSrcSizes(data);

	extendedFile.setData (this.replaceOldSizeWithNewSize (data, Size.toRegex(old_size), size));
	return extendedFile;
}

BannerCloner.prototype.replaceOldSizeWithNewSize = function (data, old_size_regex, size) {
	return data
		.replace (old_size_regex.width, function ($0, $1, $2, $3, $4) {
			return $1 + ": " + $2 + Size.getWidth(size) + $4;
		})
		.replace (old_size_regex.height, function ($0, $1, $2, $3, $4) {
			return $1 + ": " + $2 + Size.getHeight(size) + $4;
		});
}

//////////////////////////////////////////////////////////
///                        REGEX                       ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.isProjectZip = function (value, project) {
	var regex = new RegExp ('^' + project + '-[0-9]+x[0-9]+\.zip$');
	return value.match(regex, "g");
}
BannerCloner.prototype.extractProjectName = function (value) {
	return value.match(/(\w+)$/, "g")[1]
}
BannerCloner.prototype.extractSrcSizes = function (data) {
	return data.match (/\.gwd-page-size\s*{[\w\s\\n:;]*width:\s*([0-9]+)px;[\w\s\\n:;]*height\s*:\s*([0-9]+)px;?[\w\s\\n:;]*}/, 'g');
}


//////////////////////////////////////////////////////////
///                        TREE                        ///
//////////////////////////////////////////////////////////

BannerCloner.prototype.deleteProjectFromTree = function (tree, proj) {
	if (tree[proj])
		delete tree[proj];
}
BannerCloner.prototype.addProjectToTree = function (tree, proj) {
	if (!tree[proj])
		tree[proj] = [];
}
BannerCloner.prototype.addDimensionToProject = function (tree, proj, file) {
	this.addProjectToTree(tree, proj);
	tree[proj].push(file);
}

module.exports = BannerCloner;