function Counter (callback) {
	this.started = false;
	this.number = 0;
	this.callback = callback;
}

Counter.prototype.increment = function () { this.number += 1; }
Counter.prototype.decrement = function () { this.number -= 1; }
Counter.prototype.queue = function (msg = '') { 
	this.increment(); 
	this.started = true;
}
Counter.prototype.dequeue = function (msg = '') { 
	if (this.started) 
		this.decrement(); 

	if (this.isComplete()) {
		this.callback();
	}
}
Counter.prototype.isComplete = function () { return this.number == 0 && this.started; }

module.exports = Counter;