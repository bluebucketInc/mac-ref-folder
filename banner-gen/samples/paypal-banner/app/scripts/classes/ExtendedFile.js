var path = require ("path");

function ExtendedFile (foldername, filename, data) {
	this.foldername = foldername;
	this.filename = filename;
	this.data = data; 
}

ExtendedFile.prototype.setFoldername = function (foldername) { this.foldername = foldername; }
ExtendedFile.prototype.setFilename = function (filename) { this.filename = filename; }
ExtendedFile.prototype.setData = function (data) { this.data = data; }
ExtendedFile.prototype.getFoldername = function () { return this.foldername; }
ExtendedFile.prototype.getFilename = function () { return this.filename; }
ExtendedFile.prototype.getData = function () { return this.data; }

ExtendedFile.prototype.getFullPath = function () {
	return path.join(this.foldername, this.filename);
}
ExtendedFile.extractFolder = function (filepath) {
	return filepath.replace (/((\/|\\)(\w+\.\w+))$/g, "");
}
ExtendedFile.extractFile = function (filepath) {
	return filepath.match (/((\/|\\)(\w+\.\w+))$/, "g")[0];
}

module.exports = ExtendedFile;