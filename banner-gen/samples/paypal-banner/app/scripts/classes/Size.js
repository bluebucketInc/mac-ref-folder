function Size () {}
Size.fromString = function (string) {
	return string.match (/([0-9]+)x([0-9]+)/, "g");
}
Size.getWidth = function (size) { return size[1]; }
Size.getHeight = function (size) { return size[2]; }
Size.isValid = function (size) { 
	if (!size) 
		return false;

	if (Array.isArray(size))
		return (size[1] && size[2]);

	if (typeof size == "string")
		return Size.isValid (Size.fromString(size));

	return false;
}
Size.toRegex = function (size) {
	var regex = {};
	regex.width = new RegExp("(left|width): *(-*)(" + Size.getWidth(size) + ")(px)", "g");
	regex.height = new RegExp("(top|height): *(-*)(" + Size.getHeight(size) + ")(px)", "g");
	return regex;
}
module.exports = Size;