var fs = require ("fs");
var path = require ("path");
var Counter = require ("./Counter");
var ExtendedFile = require ("./ExtendedFile");

function ExtendedFileSystem () {}

ExtendedFileSystem.clone = function (src, target, handler, deeply = false) {
	if (!Array.isArray(target)) {
		target = [target];
	}

	var promise = new Promise (function (resolve, reject) {
		var method;
		fs.stat(src, function (err, stats) {
			if (err) {
				return reject(err);
			}

			if (stats.isDirectory()) {
				method = ExtendedFileSystem.cloneDirMultiple;
			} else if (stats.isFile ()) {
				if (deeply) {
					method = ExtendedFileSystem.cloneFileMultipleDeeply;
				} else {
					method = ExtendedFileSystem.cloneFileMultiple;
				}
			}

			return resolve (method);
		});
	})

	return promise.then(function(fn) {
		return fn (src, target, handler)
	})
	
}

ExtendedFileSystem.cloneDir = function (src, target, handler, counter) {
	var src_path;
	var target_path;

	var promise = new Promise (function (resolve, reject) {
		fs.mkdir(target, function (err) {
			if (err && err.code != "EEXIST") 
				return reject(err);
			fs.readdir(src, function (err, files) {
				if (err)
					return reject(err);

				resolve(files);
			})
		})
	});

	return promise.then (function (files) {

		var promises = [];
		files.forEach (function (file) {
			if (ExtendedFileSystem.shouldIgnore(file))
				return;
			src_path = path.join(src, file);
			target_path = path.join(target, handler(new ExtendedFile(target, file, null)).getFilename());
			promises.push(ExtendedFileSystem.clone (src_path, target_path, handler));
		});

		return Promise.all(promises);
	})
	
}

ExtendedFileSystem.cloneDirMultiple = function (src, targets, handler) {
	var promises = []
	targets.forEach (function (target) {
		promises.push(ExtendedFileSystem.cloneDir (src, target, handler));
	})

	return Promise.all(promises);
}

ExtendedFileSystem.cloneFile = function (src, target, handler) {
	return ExtendedFileSystem.cloneFileMultiple (src, [target], handler);
}

ExtendedFileSystem.cloneFileMultiple = function (src, targets, handler) {
	if (ExtendedFileSystem.isProcessableFile (src)) {
		return ExtendedFileSystem.cloneAndProcessFileMultiple (src, targets, handler);
	} else {
		return ExtendedFileSystem.cloneWithoutProccessFileMultiple (src, targets, handler);
	}
}

ExtendedFileSystem.cloneAndProcessFileMultiple = function (src, targets, handler) {
	return ExtendedFileSystem.readFile(src).then(function (data) {
		var promises = []
		targets.forEach (function (target) {
			promises.push(
				ExtendedFileSystem.writeFile (
					target, 
					handler (
						new ExtendedFile (
							ExtendedFile.extractFolder(target), 
							ExtendedFile.extractFile(target), 
							data
						)
					).getData()
			));
		});

		return Promise.all(promises);
	})
}

ExtendedFileSystem.cloneWithoutProccessFileMultiple = function (src, targets, handler) {
	var rs = fs.createReadStream (src);
	var self = this;
		
	return new Promise (function (resolve, reject) {
		rs.on ('end', function () {
			resolve ();
		});
		targets.forEach (function (target) {
			var ws = fs.createWriteStream (target);
			self.writeFileLog(target);
			rs.pipe(ws);
		});
	})
}


ExtendedFileSystem.cloneFileDeeply = function (src, target, handler) {
	return ExtendedFileSystem.cloneFileMultipleDeeply (src, [target], handler);
}

ExtendedFileSystem.cloneFileMultipleDeeply = function (src, targets, handler, callback) {
	return ExtendedFileSystem.readFile (src).then(function (data) {
		var promises = [];
		targets.forEach (function (target) {
			promises.push(
				ExtendedFileSystem.deepCreateAndWrite (
					target, 
					handler(
						new ExtendedFile(
							ExtendedFile.extractFolder(target), 
							ExtendedFile.extractFile(target),
							data)
					).getData()
				)
			);
		});

		return Promise.all(promises);
	});
}


ExtendedFileSystem.readFile = function (src) {
	return new Promise (function (resolve, reject) {
		fs.open (src, 'r', function (err, fd) {
			if (err)
				return reject(err);
			fs.readFile (fd, "utf8", function (err, data) {
				if (err)
					return reject(err);
				fs.close (fd, function (err) { 
					if (err) 
						return reject (err);
					resolve (data); 
				})
			});
		})
	})
}

ExtendedFileSystem.writeFile = function (src, data, callback) {
	var self = this;

	return new Promise (function (resolve, reject) {
		ExtendedFileSystem.deepOpen (src, 'w', function (err, fd) {
			if (err)
				return reject(err);
			fs.writeFile (fd, data, function (err, written, string) {
				if (err)
					return reject(err);
				fs.close (fd, function (err) { 
					if (err) 
						return reject (err); 
					resolve (src);
				})
				self.writeFileLog(src);
			});
		})
	})
	
}

ExtendedFileSystem.writeFileLog = function (filename) {
	console.log ("FILE WRITTEN: " + filename.replace (/\//g, "\\"));
}

ExtendedFileSystem.deepCreateAndWrite = function (filename, values = "", dir = './', err) {
	var path = require ("path");
	if (typeof filename == "string") {
		filename = filename.replace(/\\/g, "/").split("/");
	}

	var promise = new Promise (function (resolve, reject) {
		if (filename.length < 1) {
			return reject("INVALID FILENAME");
		} 
		if (filename.length == 1) {
			if (filename[0].trim().length > 0)
				return resolve (function () { return ExtendedFileSystem.createAndWrite (dir + filename, values) });
			else 
				reject (err);
		} else if (filename[0].trim().length == 0) {
			filename.shift();
			return resolve( function () { return ExtendedFileSystem.deepCreateAndWrite (filename, values, dir + '/') });
		} else {
			dir = path.join(dir, filename[0].trim());

			return resolve (function () {
				return new Promise(function (resolve, reject) {
					fs.mkdir (dir, function (err) {
						if (err && !ExtendedFileSystem.dirExist(err)) 
							return reject(err);
						filename.shift();
						return resolve (function () { ExtendedFileSystem.deepCreateAndWrite (filename, values, dir + '/', err) });
					})
				});
			});
			
		}
	})

	return promise.then (function (fn) { return fn(); });
	
}

ExtendedFileSystem.createAndWrite = function (filename, values, callback) {
	return new Promise (function (resolve, reject) {
		ExtendedFileSystem.deepOpen (filename, 'w', function (open_err, fd) {
			fs.writeFile(fd, values, function (write_err, written, string) {
				if(write_err) 
					return reject(write_err);

				fs.close(fd, function (err) {
					if (err)
						reject(write_err);
					resolve ();
				})
			})
		});
	})
	
}


// PRE: err is either null or contains error object
// POST: returns true if either there is no error or 
//		 if error says that directory exist
ExtendedFileSystem.dirExist =  function (err) {
	return ExtendedFileSystem.fileExist(err);
}

// PRE: err is either null or contains error object
// POST: returns true if either there is not error or 
//		 if error says that file exist
ExtendedFileSystem.fileExist = function (err) {
	return (!err || err.code != "ENOENT");
}

ExtendedFileSystem.isProcessableFile = function (filename) {
	return filename.match (/js|html|xml$/, "g")
}
ExtendedFileSystem.shouldIgnore = function (filename) {
	return filename.match (/(\.DS_Store|\.gitignore)/, "g")
}
ExtendedFileSystem.deepOpen = function (file, flag, callback) {
	fs.open (file, flag, function (err, fd) {
		if (!ExtendedFileSystem.fileExist (err)) {
			var up = ExtendedFileSystem.upOneLevel(file);
			return ExtendedFileSystem.deepMkdir (up, function () {
				ExtendedFileSystem.deepOpen (file, flag, callback);
			})
		} 
		if (err) {
			return console.log (err);
		}

		callback (err, fd);
	})
}
ExtendedFileSystem.deepMkdir = function (dir, callback) {
	fs.mkdir (dir, function (err) {
		if (!ExtendedFileSystem.dirExist(err)) {
			var up = ExtendedFileSystem.upOneLevel(file);
			return ExtendedFileSystem.deepMkdir (up, function () {
				ExtendedFileSystem.deepMkdir (dir, callback);
			})
		}
		if (err) {
			return console.log (err);
		}

		callback (err);
	})
}
ExtendedFileSystem.upOneLevel = function (filepath) {
	return filepath.replace (/(\\|\/)\w+(\.\w+)?$/g, "");
}
module.exports = ExtendedFileSystem;