//
// Available modules include (this is not a complete list):
// var Scene = require('Scene');
// var Textures = require('Textures');
// var Materials = require('Materials');
// var FaceTracking = require('FaceTracking');
// var Animation = require('Animation');
// var Reactive = require('Reactive');

// Example script
//
// Loading required modules
// var Scene = require('Scene');
const FaceTracking = require('FaceTracking');
const Scene = require('Scene');
const Time = require('Time');

const fd = Scene.root.child('Device').child('Camera').child('Focal Distance');

// EXAMPLE: Monitor facial features to trigger new activity.
var particleEmitter = fd.child('emitter');
function trackMouthOpen() {
	var face = FaceTracking.face(0);
	face.mouth.openness.multiTrigger(0.3).subscribe(function(e) {
		particleEmitter.birthrate = 50;
		Time.setTimeout(function(e) {
			particleEmitter.birthrate = 0;
		}, 1000);
	});
}

trackMouthOpen();

FaceTracking.count.monitor().subscribe(function (updatedCount) {
	if (updatedCount.newValue > 0) {
		trackMouthOpen();
	}
});